IMAGE_TAG=$(docker run --rm alpine date -u +%Y%m%d%H%M%S)
IMAGE="registry.k3d.local.with-humans.org:5003/hello-java:${IMAGE_TAG}"
docker buildx build --push -t ${IMAGE} .
echo ${IMAGE}
