FROM maven:3.9.9-eclipse-temurin-23-noble AS builder

WORKDIR /opt/app
COPY . .
RUN mvn package


FROM eclipse-temurin:23
COPY --from=builder /opt/app/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
