The example includes a ready-to-use CI/CD pipeline built using argo-workflows
and argo-cd. In a real-world situation, we would want to trigger
this pipeline on every push via a webhook. 

Since we're currently using git repositories in the cloud to collaborate, they
cannot easily reach our cluster, so we have to trigger manually:

```
argo submit -n argo hello-java-cicd.yaml
```

Use this on every commit to continuously deploy your service:

```
git commit && git push && argo submit -n argo hello-java-cicd.yaml
```

To see what's going on, you can use the argo cli or (web
interface)[https://argo-workflows.k3d.local.with-humans.org]

```
argo watch -n argo @latest
argo logs -n arg @latest --follow
```
